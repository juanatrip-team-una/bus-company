import React, { Component } from "react";
import { Table } from "react-bootstrap";
import "./Timetable.css";

export default class Timetable extends Component {
  render() {
    return (
      <div>
          <h1 align="center"> Timetable </h1>
          <Table striped bordered condensed hover responsive>
            <thead>
              <tr>
                <th>#</th>
                <th>From</th>
                <th>To</th>
                <th>Bus No</th>
                <th>Bus Type</th>
                <th>Depart Time</th>
                <th>Arrival Time</th>
                <th>Fare</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Manila</td>
                <td>Laoag</td>
                <td>2</td>
                <td>Ordinary (2x2)</td>
                <td>7:30 PM</td>
                <td>5:30 AM</td>
                <td>Php 600</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Laoag</td>
                <td>Manila</td>
                <td>5</td>
                <td>Deluxe (2x1)</td>
                <td>9:30 PM</td>
                <td>8:30 AM</td>
                <td>Php 650</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Manila</td>
                <td>Laoag</td>
                <td>14</td>
                <td>Super Deluxe (1x1x1)</td>
                <td>6:30 PM</td>
                <td>4:30 AM</td>
                <td>Php 700</td>
              </tr>
              <tr>
                <td>4</td>
                <td>Cubao</td>
                <td>Laoag</td>
                <td>27</td>
                <td>Sleeper (1x1)</td>
                <td>8:30 PM</td>
                <td>4:30 AM</td>
                <td>Php 900</td>
              </tr>
            </tbody>
          </Table>
      </div>
    );
  }
}