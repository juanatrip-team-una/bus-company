import React, { Component } from "react";
import "./Schedule.css";
import { Table } from "react-bootstrap";

export default class Schedule extends Component {
  render() {
    return (
      <div>
          <h1 align="center"> Schedules </h1>
          <Table striped bordered condensed hover responsive>
            <thead>
              <tr>
                <th>#</th>
                <th>From</th>
                <th>To</th>
                <th>Bus No</th>
                <th>Bus Type</th>
                <th>Depart Time</th>
                <th>Arrival Time</th>
                <th>Available Seats</th>
                <th>Travel Date</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Manila</td>
                <td>Laoag</td>
                <td>2</td>
                <td>Ordinary (2x2)</td>
                <td>7:30 PM</td>
                <td>5:30 AM</td>
                <td>20</td>
                <td>May 24, 2018</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Laoag</td>
                <td>Manila</td>
                <td>5</td>
                <td>Deluxe (2x1)</td>
                <td>9:30 PM</td>
                <td>8:30 AM</td>
                <td>25</td>
                <td>May 24, 2018</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Manila</td>
                <td>Laoag</td>
                <td>14</td>
                <td>Super Deluxe (1x1x1)</td>
                <td>6:30 PM</td>
                <td>4:30 AM</td>
                <td>5</td>
                <td>May 25, 2018</td>
              </tr>
              <tr>
                <td>4</td>
                <td>Cubao</td>
                <td>Laoag</td>
                <td>27</td>
                <td>Sleeper (1x1)</td>
                <td>8:30 PM</td>
                <td>4:30 AM</td>
                <td>9</td>
                <td>May 26, 2018</td>
              </tr>
            </tbody>
          </Table>
      </div>
    );
  }
}