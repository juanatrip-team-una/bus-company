import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./containers/Home";
import Schedule from "./containers/Schedule";
import Timetable from "./containers/Timetable";
import NotFound from "./containers/NotFound";

export default () =>
  <Switch>
    <Route path="/" exact component={Home} />
    <Route path="/schedule" exact component={Schedule} />
    <Route path="/timetable" exact component={Timetable} />
    { /* Finally, catch all unmatched routes */ }
    <Route component={NotFound} />
  </Switch>;