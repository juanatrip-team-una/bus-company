# base image
FROM node:9.6.1

# set working directory
RUN mkdir /app
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

COPY . /app

# install and cache app dependencies
COPY ["package.json", "package-lock.json*", "./"]
RUN npm install

EXPOSE 3008

# start app
CMD ["npm", "start"]
